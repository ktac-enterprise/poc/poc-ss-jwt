package com.klex.pocssjwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocSsJwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocSsJwtApplication.class, args);
	}

}
