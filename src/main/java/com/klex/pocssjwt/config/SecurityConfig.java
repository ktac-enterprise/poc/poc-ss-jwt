package com.klex.pocssjwt.config;

import jakarta.servlet.Filter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * Copyright (c) 2022, KLEX-ENT, All Right Reserved.</br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a></br>
 * ---</br>
 * When : 2/5/2023-- 8:25 PM</br>
 * By : @author alexk</br>
 * Project : poc-ss-jwt</br>
 * Package : com.klex.pocssjwt.config</br>
 */
@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig {

    private final JwtAuthenticationFilter jwtAuthFilter;
    private final AuthenticationProvider authenticationProvider;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
       http
           .csrf()
           .disable()
           .authorizeHttpRequests()
           .requestMatchers("/api/v1/auth/**").permitAll()
           .anyRequest().authenticated()
           .and()
           .sessionManagement()
           .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
           .and()
           .authenticationProvider(authenticationProvider)
           .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class);

        return http.build();
    }

}
