package com.klex.pocssjwt.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Copyright (c) 2022, KLEX-ENT, All Right Reserved.</br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a></br>
 * ---</br>
 * When : 2/5/2023-- 7:32 PM</br>
 * By : @author alexk</br>
 * Project : poc-ss-jwt</br>
 * Package : com.klex.pocssjwt.config</br>
 */
@Service
public class JwtService {

    private static final String SECRET_KEY = "4226452948404D635166546A576E5A7234753778214125442A462D4A614E6452";

    /**
     * Extraction of the username
     * @param jwt
     * @return the username
     */
    public String extractUsername(String jwt) {
        return extractClaims(jwt, Claims::getSubject);
    }

    /**
     * Getting Claims from Jwt
     * @param jwt
     * @return
     */
    private Claims extractAllClaims(String jwt) {
        return Jwts
                .parserBuilder()
                .setSigningKey(getSignInKey())
                .build()
                .parseClaimsJws(jwt)
                .getBody();
    }

    /**
     * Extract Claims From Jwt
     * @param jwt
     * @param claimsResolver
     * @return The claims (Ex: Username)
     * @param <T>
     */
    public <T> T extractClaims(String jwt, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(jwt);
        return claimsResolver.apply(claims);
    }

    /**
     * Generate Token without extra claims
     * @param userDetails
     * @return the token
     */
    public String generateToken(UserDetails userDetails) {
        return generateToken(new HashMap<>(), userDetails);
    }

    /**
     * Generation of Token using Extra Claims, and userDetails
     * @param extraClaims
     * @param userDetails
     * @return the token
     */
    public String generateToken(Map<String, Object> extraClaims, UserDetails userDetails) {
        return Jwts
                .builder()
                .setClaims(extraClaims)
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() +  1000 * 60 * 60 * 24))
                .signWith(getSignInKey(), SignatureAlgorithm.HS256)
                .compact();
    }

    public boolean isTokenValid(String jwt, UserDetails userDetails) {
        final String username = extractUsername(jwt);
        return (username.equals(userDetails.getUsername())) && !isTokenExpired(jwt);
    }

    /**
     * Check if the token is expired
     * @param jwt
     * @return true or false
     */
    private boolean isTokenExpired(String jwt) {
        return extractExpiration(jwt).before(new Date());
    }

    /**
     * Extract the claim Expiration Date
     * @param jwt
     * @return the expiration date
     */
    private Date extractExpiration(String jwt) {
        return extractClaims(jwt, Claims::getExpiration);
    }

    /**
     * Create a Key using our Secret HEX Key decoded in Base 64
     * @return the Key
     */
    private Key getSignInKey() {
        byte[] keyByte = Decoders.BASE64.decode(SECRET_KEY);
        return Keys.hmacShaKeyFor(keyByte);
    }
}
